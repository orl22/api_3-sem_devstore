# Sprint 1

<img src="/Imagens_Sprint1/logo.jpg"/>

##  :computer: proposta

Na primeira sprint temos o objetivo em desenvolver a tela planning Pokker, e com a criação desta tela estabelecer a função de criar uma atividade planning com persistencia de dados.E com isso houve a criação,do modelo logico e fisico do banco de dados das atividades.Além disso, foi feito uma tela de cadastro do Scrum Master para integração de dados no banco e Com tal tela de login realizar a consulta de dados para sua validação.

##  :chart_with_upwards_trend: Meta da Sprint

A meta desejada em  atingir com a Sprint 1 é o inicio da montagem de um sistema de Votação Planning Pokker com os requisitos chaves a serem utilizados para o desenvolvimento de uma futura home page com funcionamento simples e dinâmico;


##  :triangular_flag_on_post: Detalhes da Sprint:
**Resumo de Implementações:**

- Wireframes;
- Banco de Dados;
- Implementação do Banco de Dados;
- Implementação do Back-end:
- Login;
- Cadastro de usuários;
- Integração com o Front-end;
- Definição e implementação das regras de negócio;



## Apresentação da Sprint:
<a href="https://gitlab.com/orl22/api_3-sem_devstore/-/blob/main/Imagens_Sprint1/API_-_Apresentacao_-_Sprint_1__5_.pptx">Apresentação PowerPoint</a>



## WireFrames:
<img src="/Imagens_Sprint1/Tela_inicial_Sprint1.jpeg"/>

 <img src="/Imagens_Sprint1/Tela_cad_sprint1.jpeg"/>
 

<img src="/Imagens_Sprint1/BD_Sprint1.jpeg"/>





## :department_store: Proposta da proxima Sprint: 
- Para proxima Sprint temos como objetivo desenvolver os comandos Inserir, listar e remover atividades na tela Planning Poker e criar equipe com persistência de dados;
